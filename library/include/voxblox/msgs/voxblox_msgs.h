#ifndef VOXBLOX_MSGS_H_
#define VOXBLOX_MSGS_H_

#include <stdint.h>
#include <string>
#include <vector>

namespace voxblox_msgs {

typedef struct Block {
    int32_t x_index;
    int32_t y_index;
    int32_t z_index;
    std::vector<uint32_t> data;
} Block;

// uint8_t ACTION_UPDATE = 0;
// uint8_t ACTION_MERGE = 1;
// uint8_t ACTION_RESET = 2;
typedef struct Layer {
    float voxel_size;
    uint32_t voxels_per_side;
    std::string layer_type;
    std::vector<Block> blocks;
    uint8_t action;
} Layer;


typedef struct MeshBlock{
    int64_t index[3];
    std::vector<uint16_t> x;
    std::vector<uint16_t> y;
    std::vector<uint16_t> z;
    std::vector<uint8_t> r;
    std::vector<uint8_t> g;
    std::vector<uint8_t> b;
} MeshBlock;

typedef struct Mesh{
    uint64_t ts;
    float block_edge_length;
    std::vector<MeshBlock> mesh_blocks;
} Mesh;


typedef struct VoxelEvaluation {
    float max_error;
    float min_error;
    uint32_t num_evaluated_voxels;
    uint32_t num_ignored_voxels;
    uint32_t num_overlapping_voxels;
    uint32_t num_non_overlapping_voxels;
} VoxelEvaluation;

} // voxblox_msgs

#endif //VOXBLOX_MSGS_H_
