#ifndef VOXBLOX_MESH_MESH_UTILS_H_
#define VOXBLOX_MESH_MESH_UTILS_H_

#include <vector>

#include "voxblox/core/block_hash.h"
#include "voxblox/core/common.h"
#include "voxblox/mesh/mesh.h"

namespace voxblox {

/**
 * Combines all given meshes into a single mesh with connected verticies. Also
 * removes triangles with zero surface area. If you only would like to connect
 * vertices, make sure that the proximity threhsold <<< voxel size. If you would
 * like to simplify the mesh, chose a threshold greater or near the voxel size
 * until you reached the level of simpliciation desired.
 */
/// @return 
/// -1 on mesh error
/// -2 on vertex reduction error
/// -3 on index error
/// -4 on output mesh error
inline int createConnectedMesh(
    const AlignedVector<Mesh::ConstPtr>& meshes, Mesh* connected_mesh,
    const float approximate_vertex_proximity_threshold = 1e-10) {
  if(connected_mesh == nullptr){
    std::cerr << "connected_mesh is a nullptr" << std::endl;
    exit(-1);
  }

  // Used to prevent double ups in vertices. We need to use a long long based
  // index, to prevent overflows.
  LongIndexHashMapType<size_t>::type uniques;

  const double threshold_inv =
      1. / static_cast<double>(approximate_vertex_proximity_threshold);

  // Combine everything in the layer into one giant combined mesh.
  size_t new_vertex_index = 0u;
  for (const Mesh::ConstPtr& mesh : meshes) {
    // Skip empty meshes.
    if (mesh->vertices.empty()) {
      continue;
    }

    // Make sure there are 3 distinct vertices for every triangle before
    // merging.
    if(mesh->vertices.size() != mesh->indices.size()){
      std::cerr << "mesh vertices and indices are not the same size" << std::endl;
      return -1;
      // exit(-1);
    }
    if(mesh->vertices.size() % 3u != 0u){
      std::cerr << "mesh vertices size is not divisible by 3" << std::endl;
      return -1;
      // exit(-1);
    }
    if(mesh->indices.size() % 3u !=  0u){
      std::cerr << "mesh indices size is not divisible by 3" << std::endl;
      // exit(-1);
      return -1;
    }

    // Stores the mapping from old vertex index to the new one in the combined
    // mesh. This is later used to adapt the triangles to the new, global
    // indexing of the combined mesh.
    std::vector<size_t> old_to_new_indices;
    old_to_new_indices.resize(mesh->vertices.size());

    size_t new_num_vertices_from_this_block = 0u;
    for (size_t old_vertex_idx = 0u; old_vertex_idx < mesh->vertices.size();
         ++old_vertex_idx) {
      // We scale the vertices by the inverse of the merging tolerance and
      // then compute a discretized grid index in that scale.
      // This exhibits the behaviour of merging two vertices that are
      // closer than the threshold.
      if(old_vertex_idx >= mesh->vertices.size()){
        std::cerr << "old vertex index is >= mesh vertices size" << std::endl;
        // exit(-1);
        return -2;
      }
      const Point vertex = mesh->vertices[old_vertex_idx];
      const Eigen::Vector3d scaled_vector =
          vertex.cast<double>() * threshold_inv;
      const LongIndex vertex_3D_index = LongIndex(
          std::round(scaled_vector.x()), std::round(scaled_vector.y()),
          std::round(scaled_vector.z()));

      // If the current vertex falls into the same grid cell as a previous
      // vertex, we merge them. This is done by assigning the new vertex to
      // the same vertex index as the first vertex that fell into that cell.

      LongIndexHashMapType<size_t>::type::const_iterator it =
          uniques.find(vertex_3D_index);

      const bool vertex_is_unique_so_far = (it == uniques.end());
      if (vertex_is_unique_so_far) {
        // Copy vertex and associated data to combined mesh.
        connected_mesh->vertices.push_back(vertex);

        if (mesh->hasColors()) {
          if(old_vertex_idx >= mesh->colors.size()){
            std::cerr << "old_vertex_idx out of range for colors" << std::endl;
            // exit(-1);
            return -2;
          }
          connected_mesh->colors.push_back(mesh->colors[old_vertex_idx]);
        }
        if (mesh->hasNormals()) {
          if(old_vertex_idx >= mesh->normals.size()){
            std::cerr << "old_vertex_idx out of range for normals" << std::endl;
            // exit(-1);
            return -1;
          }
          connected_mesh->normals.push_back(mesh->normals[old_vertex_idx]);
        }

        // Store the new vertex index in the unique-vertex-map to be able to
        // retrieve this index later if we encounter vertices that are
        // supposed to be merged with this one.
        uniques.emplace(vertex_3D_index, new_vertex_index).second;

        // Also store a mapping from old index to new index for this mesh
        // block to later adapt the triangle indexing.
        if(old_vertex_idx >= old_to_new_indices.size()){
          std::cerr << "old vertex idx cannot be mapped to new indices" << std::endl;
          // exit(-1);
          return -2;
        }
        old_to_new_indices[old_vertex_idx] = new_vertex_index;

        ++new_vertex_index;
        ++new_num_vertices_from_this_block;
      } else {
        // If this vertex is not unique, we map it's vertex index to the new
        // vertex index.
        if(old_vertex_idx >= old_to_new_indices.size()){
          std::cerr << "old vertex idx cannot be mapped to new indices" << std::endl;
          // exit(-1);
          return -2;
        }
        old_to_new_indices[old_vertex_idx] = it->second;

        // Add all normals (this will average them once they are renormalized
        // later)
        connected_mesh->normals[it->second] += mesh->normals[old_vertex_idx];
      }

      // Make sure the indexing is correct.
      if(connected_mesh->vertices.size() != new_vertex_index){
        std::cerr << "connected mesh size is not equal to new vertext index" << std::endl;
        // exit(-1);
        return -2;
      }
    }

    // Renormalize normals
    for (Point& normal : connected_mesh->normals) {
      float length = normal.norm();
      if (length > coordEpsilon) {
        normal /= length;
      } else {
        normal = Point(0.0f, 0.0f, 1.0f);
      }
    }

    // Make sure we have a mapping for every old vertex index.
    if(old_to_new_indices.size() != mesh->vertices.size()){
      std::cerr << "index and vertext sizes do not match" << std::endl;
      // exit(-1);
      return -2;
    }

    // Append triangles and adjust their indices if necessary.
    // We discard triangles where all old vertices were mapped to the same
    // vertex.
    size_t new_num_triangle_from_this_block = 0u;
    for (size_t triangle_idx = 0u; triangle_idx < mesh->indices.size();
         triangle_idx += 3u) {
      if(triangle_idx + 2u >= mesh->indices.size()){
        std::cerr << "triangle index out of range" << std::endl;
        // exit(-1);
        return -3;
      }

      // Retrieve old vertex indices.
      size_t vertex_0 = mesh->indices[triangle_idx];
      size_t vertex_1 = mesh->indices[triangle_idx + 1u];
      size_t vertex_2 = mesh->indices[triangle_idx + 2u];

      // Make sure the old indices were valid before remapping.
      if(vertex_0 >= old_to_new_indices.size()){
        std::cerr << "vertex 0 out of range for indices" << std::endl;
        // exit(-1);
        return -3;
      }
      if(vertex_1 >= old_to_new_indices.size()){
        std::cerr << "vertex 1 out of range for indices" << std::endl;
        // exit(-1);
        return -3;
      }
      if(vertex_2 >= old_to_new_indices.size()){
        std::cerr << "vertex 2 out of range for indices" << std::endl;
        // exit(-1);
        return -3;
      }

      // Apply vertex index mapping.
      vertex_0 = old_to_new_indices[vertex_0];
      vertex_1 = old_to_new_indices[vertex_1];
      vertex_2 = old_to_new_indices[vertex_2];

      // Make sure the new indices are valid after remapping.
      if(vertex_0 >= new_vertex_index){
        std::cerr << "vertex 0 not valid after remapping" << std::endl;
        // exit(-1);
        return -3;
      }
      if(vertex_1 >= new_vertex_index){
        std::cerr << "vertex 1 not valid after remapping" << std::endl;
        // exit(-1);
        return -3;
      }
      if(vertex_2 >= new_vertex_index){
        std::cerr << "vertex 2 not valid after remapping" << std::endl;
        // exit(-1);
        return -3;
      }

      // Get rid of triangles where all two or three vertices have been
      // merged.
      const bool two_or_three_vertex_indices_are_the_same =
          (vertex_0 == vertex_1) || (vertex_1 == vertex_2) ||
          (vertex_0 == vertex_2);

      if (!two_or_three_vertex_indices_are_the_same) {
        connected_mesh->indices.push_back(vertex_0);
        connected_mesh->indices.push_back(vertex_1);
        connected_mesh->indices.push_back(vertex_2);
        ++new_num_triangle_from_this_block;
      }
    }
  }

  // Verify combined mesh.
  if (connected_mesh->hasColors()) {
    if(connected_mesh->vertices.size() != connected_mesh->colors.size()){
        std::cerr << "vertices size is != to colors size" << std::endl;
        // exit(-1);
        return -4;
    }
  }
  if (connected_mesh->hasNormals()) {
    if(connected_mesh->vertices.size() != connected_mesh->normals.size()){
        std::cerr << "vertices size != normals size" << std::endl;
        // exit(-1);
        return -4;
    }
  }
  return 0;
}

inline int createConnectedMesh(
    const Mesh& mesh, Mesh* connected_mesh,
    const float approximate_vertex_proximity_threshold = 1e-10) {
  AlignedVector<Mesh::ConstPtr> meshes;
  meshes.push_back(Mesh::ConstPtr(&mesh, [](Mesh const*) {}));
  return createConnectedMesh(meshes, connected_mesh,
                      approximate_vertex_proximity_threshold);
}

};  // namespace voxblox

#endif  // VOXBLOX_MESH_MESH_UTILS_H_
